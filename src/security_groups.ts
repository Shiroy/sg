import * as aws from "@pulumi/aws";
import * as pulumi from "@pulumi/pulumi";

export interface PortRange {
  from: number;
  to: number
}

export type PortDefinition = number | number[] | PortRange;

export interface ITcpUdpStream {
  protocol: "tcp" | "udp";
  port: PortDefinition
}

export interface IICMPStream {
  protocol: "icmp",
  icmpMessage: number
}

export interface IOtherStream {
  protocol: "other"
  protocol_number: number
}

export type IStream = ITcpUdpStream | IICMPStream | IOtherStream;

function isPortList(o: PortDefinition): o is number[] {
  return "length" in (o as any);
}

function isPortRange(o: PortDefinition): o is PortRange {
  // Let's be specific
  return "from" in (o as any)
    && typeof (o as any)["from"] === "number"
    && "to" in (o as any)
    && typeof (o as any)["to"] === "number";
}

interface SecurityGroupRule {
  protocol: string;
  fromPort: number;
  toPort: number;
}

function toSecurityGroupRule(stream: IStream): SecurityGroupRule[] {
  if (stream.protocol == "tcp" || stream.protocol == "udp") {
    if (typeof stream.port === "number") {
      return [{
        protocol: stream.protocol,
        fromPort: stream.port,
        toPort: stream.port
      }];
    }
    else if (isPortList(stream.port)) {
      return stream.port.map(port => {
        return {
          protocol: stream.protocol,
          fromPort: port,
          toPort: port
        };
      });
    }
    else if (isPortRange(stream.port)) {
      return [{
        protocol: stream.protocol,
        fromPort: stream.port.from,
        toPort: stream.port.to
      }];
    } else {
      throw Error("unreachable"); //TODO: Check how to handle this case in a javascript way
    }
  } else {
    return [];
  }
}

export type SecurityGroupCtor = new (...args: any[]) => aws.ec2.SecurityGroup;

export interface IStreamConnector {
  unidirectionalTo<T extends aws.ec2.SecurityGroup>(name: string, to: T, stream: IStream): void;
  bidirectionalTo<T extends aws.ec2.SecurityGroup & IStreamConnector>(name: string, to: T, stream: IStream): void;
}

export function StreamConnector<TBase extends SecurityGroupCtor>(Base: TBase) {
  return class extends Base implements IStreamConnector {
    unidirectionalTo<T extends aws.ec2.SecurityGroup>(name: string, to: T, stream: IStream) {
      let ruleDef = toSecurityGroupRule(stream);
      ruleDef.map((rule, index) => this._unidirectional(`${name}-${index}`, rule.protocol, rule.fromPort, rule.toPort, to));
    }

    bidirectionalTo<T extends aws.ec2.SecurityGroup & IStreamConnector>(name: string, to: T, stream: IStream) {
      this.unidirectionalTo(`${name}-AToB`, to, stream);
      to.unidirectionalTo(`${name}-BToA`, this, stream);
    }

    /// DO NOT CALL THIS DIRECTLY
    _unidirectional<T extends aws.ec2.SecurityGroup>(name: string, protocol: string, from: number, to: number, other: T) {
      new aws.ec2.SecurityGroupRule(`${name}-egress`, {
        protocol: protocol,
        fromPort: from,
        toPort: to,
        securityGroupId: this.id,
        sourceSecurityGroupId: other.id,
        type: "egress"
      }, {
        parent: this
      });

      new aws.ec2.SecurityGroupRule(`${name}-ingress`, {
        protocol: protocol,
        fromPort: from,
        toPort: to,
        securityGroupId: other.id,
        sourceSecurityGroupId: this.id,
        type: "ingress"
      }, {
        parent: other
      });
    }
  }
}
